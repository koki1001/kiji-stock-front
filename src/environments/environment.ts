// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  amplify: {
    // AWS Amplify(Auth)の設定
    Auth: {
      region: 'us-east-2',
      userPoolId: 'us-east-2_xoOFrfZG6',
      userPoolWebClientId: '6b8ve270sjn2t4dkdl68779kkr'
    }
  },
  // API Gatewayのエンドポイントの設定
  apiBaseUrl: 'https://90xc3y5eec.execute-api.us-east-2.amazonaws.com/test',
  // Localstorageの設定
  localstorageBaseKey: 'CognitoIdentityServiceProvider.<6b8ve270sjn2t4dkdl68779kkr>.'
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
