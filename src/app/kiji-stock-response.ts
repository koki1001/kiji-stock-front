/**
 * KijiStock Response Class.
 */
export class KijiStockResponse {
    result: string;
    message: string;
    data: any;
}