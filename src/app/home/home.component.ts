import { Component, OnInit } from '@angular/core';
import { AuthService } from './../service/auth/auth.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { StockItem } from '../stock-item';
import { Folder } from '../folder';
import { StockItemService } from '../service/stock-item.service';
import { FolderService } from '../service/folder.service';
import { Jsonp } from '@angular/http';
import { KijiStockResponse } from '../kiji-stock-response';

@Component({
    providers: [Jsonp],
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
    kijiStockResponse: KijiStockResponse; // KijiStock Response

    stockItemList: StockItem[]; // Stock Item List
    folderList: Folder[]; // Folder List

    userId: string; // User ID
    userName: string; // User Name
    folderId: string; // Folder ID
    folderName: string; // Folder Name

    stockItemForm: FormGroup; // Stock Item Form
    addFolderForm: FormGroup; // Add Folder Form    

    constructor(private fb: FormBuilder, private auth: AuthService, private stockItemService: StockItemService, private folderService: FolderService) {
        this.stockItemForm = this.fb.group({
            inputUrl: '',
            selectFolderName: ''
        });
        this.addFolderForm = this.fb.group({
            inputFolderName: ''
        });
    }

    ngOnInit(): void {
        this.getInitialData();
    }

    /**
     * Get Initial Data.
     */
    getInitialData(): void {
        this.auth.getData().subscribe(
            result => {
                // Set User Info
                this.userId = result.attributes.sub;
                this.userName = result.username;
                // Get Folder List
                this.folderService.getFolderList(this.userId).then((response) => {
                    if (response.data == '') {
                        return;
                    }
                    // Set Folder Info
                    this.stockItemForm.controls.selectFolderName.setValue(response.data[0].folderId);
                    this.folderId = this.stockItemForm.controls.selectFolderName.value;
                    this.folderName = response.data[0].name;
                    this.setFolderList(response)
                    error => alert('Fail to get folder list: ' + error)

                    // Get Stock Item List
                    this.stockItemService.getStockItemList(this.userId, this.folderId).then((response) => {
                        // Set Stock Item Info
                        this.setStockItemList(response)
                        error => alert('Fail to get stock item list: ' + error)
                    });
                });
            },
            error => {
                alert('Unexpected error occured: ' + error)
            }
        );
    }

    /**
     * Set list of stock item info.
     * 
     * @param result
     */
    setStockItemList(result): void {
        if (result.error) {
            alert('Web API error: ' + result.message);
            return;
        }
        this.kijiStockResponse = result;
        this.stockItemList = this.kijiStockResponse.data;
    }

    /**
     * Stock item.
     */
    stockItem(): void {
        let url = this.stockItemForm.controls.inputUrl;
        if (url.value === null || url.value === '') {
            alert('Please paste a url');
            return;
        }
        let folderName = this.stockItemForm.controls.selectFolderName.value;
        this.stockItemService.stockItem(this.userId, folderName, url.value).then((response) => {
            this.setStockItemList(response),
                error => alert('Connection error: ' + error)
        });
        url.setValue(null);
    }

    /**
     * Delete stock item.
     * 
     * @param event
     */
    deleteStockItem(event: any): void {
        if (confirm('Are you sure?')) {
            let folderName = this.stockItemForm.controls.selectFolderName.value;
            this.stockItemService.deleteStockItem(this.userId, folderName, event.target.alt).then((response) => {
                this.setStockItemList(response),
                    error => alert('Connection error: ' + error)
            });
        }
    }

    /**
     * Set list of folder info.
     * 
     * @param result
     */
    setFolderList(result): void {
        if (result.error) {
            alert('Web API error: ' + result.message);
            return;
        }
        this.kijiStockResponse = result;
        this.folderList = this.kijiStockResponse.data;
    }

    /**
     * Add folder.
     */
    addFolder(): void {
        let folderName = this.addFolderForm.controls.inputFolderName;
        if (folderName.value === null || folderName.value === '') {
            alert('Please enter a folder name');
            return;
        }
        this.folderService.addFolder(this.userId, folderName.value).then((response) => {
            this.setFolderList(response),
                error => alert('Connection error: ' + error)
        });
        folderName.setValue(null);
    }

    /**
     * Delete folder.
     * 
     * @param event
     */
    deleteFolder(event: any): void {
        if (confirm('Are you sure?')) {
            this.folderService.deleteFolder(this.userId, event.target.alt).then((response) => {
                this.setFolderList(response),
                    error => alert('Connection error: ' + error)
            });
        }
    }

    /**
     * Change folder if click any folder name.
     * 
     * @param event 
     */
    changeFolder(event: any): void {
        let title = event.target.title;
        if (title === "") {
            this.folderName = event.target.parentNode.title;
            this.folderId = event.target.parentNode.id;
        } else {
            this.folderName = event.target.title;
            this.folderId = event.target.id;
        }
        this.stockItemService.getStockItemList(this.userId, this.folderId).then((response) => {
            this.setStockItemList(response)
            error => alert('Connection error: ' + error)
        });
    }
}