/**
 * Stock Item Class.
 */
export class StockItem {
    id : string;
    url : string;
    title : string;
    content : string;
    faviconPath : string;
}