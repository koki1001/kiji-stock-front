import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

@Injectable()
export class StockItemService {

  //StockItem EndpointのベースURL
  private baseUrl = '/api/item';

  //コンストラクタで利用するモジュールをインスタンス化
  constructor(private http: HttpClient) { }

  /**
   * ストックアイテムリストを取得する.
   * 
   * @param userId 
   */
  getStockItemList(userId: string, folderId:string): Promise<any> {
    return this.http.get(this.baseUrl + '/list', { headers: { 'userId': userId }, params: { 'folderId': folderId } })
      .toPromise().then((response) => {
        return response;
      });
  }

  /**
   * アイテムをストックする.
   * 
   * @param userId
   * @param url 
   */
  stockItem(userId: string, folderId: string, url: string): Promise<any> {
    return this.http.post(this.baseUrl + '/stock', { userId: userId, folderId: folderId, url: url })
      .toPromise().then((response) => {
        // URL一括取得を再度行う
        return this.getStockItemList(userId, folderId);
      });
  }

  /**
   * ストックアイテムを削除する.
   * 
   * @param userId
   * @param stockItemId 
   */
  deleteStockItem(userId: string, folderId: string, stockItemId: string): Promise<any> {
    return this.http.delete(this.baseUrl + '/delete', { params: { 'stockItemId': stockItemId } })
      .toPromise().then((response) => {
        console.log(response)
        // URL一括取得を再度行う
        return this.getStockItemList(userId, folderId);
      });
  }
}