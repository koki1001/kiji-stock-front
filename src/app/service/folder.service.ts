import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

@Injectable()
export class FolderService {

  //StockItem EndpointのベースURL
  private baseUrl = '/api/folder';

  //コンストラクタで利用するモジュールをインスタンス化
  constructor(private http: HttpClient) { }

  /**
   * フォルダリストを取得する.
   * 
   * @param userId 
   */
  getFolderList(userId: string): Promise<any> {
    return this.http.get(this.baseUrl + '/list', { headers: { 'userId': userId } })
      .toPromise().then((response) => {
        return response;
      });
  }

  /**
   * フォルダを追加する.
   * 
   * @param userId
   * @param folderName
   */
  addFolder(userId: string, folderName: string): Promise<any> {
    return this.http.post(this.baseUrl + '/add', { userId: userId, folderName: folderName })
      .toPromise().then((response) => {
        // フォルダ一括取得を再度行う
        return this.getFolderList(userId);
      });
  }

  /**
   * フォルダを削除する.
   * 
   * @param userId
   * @param folderId 
   */
  deleteFolder(userId: string, folderId: string): Promise<any> {
    return this.http.delete(this.baseUrl + '/delete', { params: { 'folderId': folderId } })
      .toPromise().then((response) => {
        console.log(response)
        // URL一括取得を再度行う
        return this.getFolderList(userId);
      });
  }
}